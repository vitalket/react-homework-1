import { useState } from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";
import './App.scss'

function App() {

  const [firstModalOpen, setFirstModalOpen] = useState(false)
  const [secondModalOpen, setSecondModalOpen] = useState(false)

  return (
    <div className="app">
      <Button backgroundColor="#2677DB" text="Open first modal" onClick={() => setFirstModalOpen(true)} />
      <Button backgroundColor="#6B26DB" text="Open second modal" onClick={() => setSecondModalOpen(true)} />

      {firstModalOpen && (
        <Modal
          header="Do you want to delete this file" 
          closeButton
          text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
          actions={
          <>
            <Button backgroundColor="rgba(0, 0, 0, 0.5)" text="OK" />
            <Button backgroundColor="rgba(0, 0, 0, 0.5)" text="Cancel" onClick={() => setFirstModalOpen(false)} />
          </>
        }
        onClose={() => setFirstModalOpen(false)}
        />
        )}

        {secondModalOpen && (
        <Modal
          header="Do you want to download this file" 
          closeButton
          text="Click Download"
          actions={
          <>
            <Button backgroundColor="black" text="Download" />
            <Button backgroundColor="black" text="Cancel" onClick={() => setSecondModalOpen(false)} />
          </>
        }
        onClose={() => setSecondModalOpen(false)}
        />
        )}

    </div>
  )
}

export default App;
